import React, { Component } from 'react';
import faker from 'faker';
import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';

class EX3 extends Component {
    render() {
        return (
            <div className="ui container comments">
                <ApprovalCard>
                    <div>
                        <h4>Warning!!!</h4>
                        Are you sure you want to do it?
                    </div>
                </ApprovalCard>
                <ApprovalCard>
                    <CommentDetail 
                        author="Alex"
                        timeAgo="Today at 5:30AM"
                        content="Nice Blog Post!!!"
                        avatar={faker.image.avatar()}
                    />
                </ApprovalCard>
                <ApprovalCard>
                    <CommentDetail 
                        author="Sam"
                        timeAgo="Today at 1:30PM"
                        content="I like it!"
                        avatar={faker.image.avatar()}
                    />
                </ApprovalCard>
                <ApprovalCard>
                    <CommentDetail 
                        author="Bill"
                        timeAgo="Today at 4:15AM"
                        content="I thing this is a good idea"
                        avatar={faker.image.avatar()}
                    />
                </ApprovalCard>
            </div>

            
        );
    }
}

export default EX3;