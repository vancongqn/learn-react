import React, { Component } from 'react';

function getTime() {
    return (new Date()).toLocaleTimeString()
}

class EX2 extends Component {
    render() {
        return (
            <div>
                <h2>Hi There!!! </h2>
                <div>Current Time:</div>
                <h3>{getTime()}</h3>
            </div>
        );
    }
}

export default EX2;