import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk'

import App from './component/App';
import reducers from './reducers';

const store = createStore(reducers, applyMiddleware(thunk));

class Blog extends Component {
    
    render() {
        return (
            <Provider store={store}>
                <App />
            </Provider>
        );
    }
}

export default Blog;