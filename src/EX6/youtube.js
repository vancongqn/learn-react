import axios from 'axios';
const KEY='AIzaSyAbnaf4sKf29XOjyAAtiwae0xsyHxLDvIA';
export const baseParams = {
    part: "snippet",
    maxResults: 5,
    key: KEY
};

export default axios.create({
    baseURL: "https://www.googleapis.com/youtube/v3"
});