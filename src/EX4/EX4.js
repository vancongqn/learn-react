import React, { Component } from 'react';
import SeasonDisplay from './SeasonDisplay';
import Spinner from './Spinner';


class EX4 extends Component {

    state = {lat:null, errorMessage:'', time: new Date().toLocaleTimeString()};

    componentDidMount() {
        window.navigator.geolocation.getCurrentPosition(
            position => {
                this.setState({lat:position.coords.latitude});
            },
            err => {
                this.setState({errorMessage: err.message});
            }
        );
        setInterval(() => {
            this.setState({time: new Date().toLocaleTimeString()})
        }, 1000);
    }

    render() {
        if(this.state.lat && !this.state.errorMessage) {
            return(
                <SeasonDisplay lat={this.state.lat}/>
            );
        };
        if(!this.state.lat && this.state.errorMessage) {
            return(
                <div>Latitude: {this.state.errorMessage}</div>
            );
        };
        return(
            <Spinner message="Please acepct location request!"/>
        );

    }
}

export default EX4;