import React, { Component } from 'react';
// import EX1 from './EX1/EX1';
// import EX2 from './EX2/EX2';
// import EX3 from './EX3/EX3';
// import EX4 from './EX4/EX4'
// import EX5 from './EX5/EX5'
// import EX6 from './EX6/EX6'
// import AppSong from './AppSong';
import Blog from './Blog'
import './App.css';



class App extends Component {
  render() {
    return (
      <div >
        {/* <EX1 /> */}
        {/* <EX2 /> */}
        {/* <EX3 /> */}
        {/* <EX4 /> */}
        {/* <EX5 /> */}
        {/* <EX6 /> */}
        {/* <AppSong /> */}
        <Blog />
      </div>
    )
  }
}

export default App;