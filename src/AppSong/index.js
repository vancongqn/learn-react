import React, { Component } from 'react';
import { Provider } from 'react-redux';
import App from './components/App';
import { createStore } from 'redux';
import reducers from './reducers';

class AppSong extends Component {
    render() {
        return (
            <Provider store={createStore(reducers)}>
                <App />
            </Provider>
            
        );
    }
}

export default AppSong;