import { combineReducers } from 'redux';

const songReducer = () => {
    return [
        {
            title: 'Den Da Khong Duong', duration: '3:55'
        },
        {
            title: 'Anh Nha O Dau The', duration: '4:02'
        },
        {
            title: 'Phut Ban Dau', duration: '3:39'
        },
        {
            title: 'Con Chim Non', duration: '2:22'
        }
    ];
};

const selectedSongReducer = (selectedSong=null,action) => {
    if(action.type === 'SONG_SELECTED'){
        return action.payload;
    }
    return selectedSong;
}

export default combineReducers({
    songs: songReducer,
    selectedSong: selectedSongReducer
});