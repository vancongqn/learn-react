import React, { Component } from 'react';
import unsplash from './Unsplash';
import SearchBar from './SearchBar';
import ImagesList from './ImagesLish';

class EX5 extends Component {

    state = {images:[]};

    onSearchSubmit = async (term) => {
       const response=  await unsplash.get('https://api.unsplash.com/search/photos', {
            params: { query:term },
            headers: {
                Authorization: 
                'Client-ID GEEv4uq589Wbyvn20d3jpuj-iq71RsOp5R3np1ADODU'
            }
        })

        this.setState({images:response.data.results});
    };

    render() {
        return (
            <div className="ui container">
                <SearchBar onSubmit={this.onSearchSubmit}/>
                <ImagesList images={this.state.images} />
            </div>
        );
    };
}

export default EX5;