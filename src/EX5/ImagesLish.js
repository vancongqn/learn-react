import React from 'react';
import './ImagesLish.css';
import ImageCard from './ImageCard';

const ImagesLish = props => {

    const images = props.images.map((image) => {
        return <ImageCard key={image.id} image={image} />
    })

    return (
        <div className="image-lish">
            {images}
        </div>
    );
};

export default ImagesLish;