import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.unsplash.com/',
    Headers: {
        Authorization: 
        'Client-ID GEEv4uq589Wbyvn20d3jpuj-iq71RsOp5R3np1ADODU'
    }
});